#pragma once
#include "config.h"
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#ifdef HAVE_STDINT_H
#include <stdint.h>
#endif
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#ifdef HAVE_TIME_H
#include <time.h>
#endif
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#ifdef HAVE_SYS_RESOURCE_H
#include <sys/resource.h>
#endif
#include <ctype.h>
#include <limits.h>
#include <float.h>

struct mpf_hmat_create_compression_args_t {
  int method;
  double threshold;
  const void * output;
};

/*! \brief Displays a warning message and that's all. */
#define SETWARN(n,s, ...)   {MpfWarning(__LINE__,PACKAGE_NAME,__FILE__,__func__,n,s, ## __VA_ARGS__);}
/*! \brief Displays an error message. */
#define SHWERRM(n,s, ...)   {MpfError(__LINE__,PACKAGE_NAME,__FILE__,__func__,n,s, ## __VA_ARGS__);}
/*! \brief Displays an error message and quits the current routine. */
#define SETERRQ(n,s, ...)   {int _ierr = MpfError(__LINE__,PACKAGE_NAME,__FILE__,__func__,n,s, ## __VA_ARGS__); return _ierr;}
/*! \brief Displays an error message and aborts the code. */
#define SETERRA(n,s, ...)   {int _ierr = MpfError(__LINE__,PACKAGE_NAME,__FILE__,__func__,n,s, ## __VA_ARGS__); exit(_ierr); }
/*! \brief Checks the integer n, if not zero then it displays an error message and quits the current routine. */
#define CHKERRQ(n)     {if (n) SETERRQ(n,(char *)0);}
/*! \brief Checks the integer n, if not zero then it displays an error message and aborts the code. */
#define CHKERRA(n)     {if (n) SETERRA(n,(char *)0);}
/*! \brief Checks the pointer p, if it is NULL then it displays an error message and quits the current routine. */
#define CHKPTRQ(p)     {if (!(p)) SETERRQ(1,"Unable to allocate requested memory");}
/*! \brief Checks the pointer p, if it is NULL then it displays an error message and aborts the code. */
#define CHKPTRA(p)     {if (!(p)) SETERRA(1,"Unable to allocate requested memory");}
/*! \brief Evaluates the expression x, if it is false then it displays an error message and quits the current routine. */
#define ASSERTQ(x) { if (!(x)) { SETERRQ(1, "Assert failure %s", #x); }}
/*! \brief Evaluates the expression x, if it is false then it displays an error message and aborts the code. */
#define ASSERTA(x) { if (!(x)) { SETERRA(1, "Assert failure %s", #x); }}

typedef struct timespec Time;

int MpfError(int line,char *package,char *file,const char *function, int number, ...) ;
int MpfWarning(int line,char *package,char *file,const char *function,int number, ...) ;
int Mpf_progressBar(int currentValue, int maximumValue) ;
int MpfArgGetDouble( int *Argc, char **argv, int rflag, const char *name, double *val ) ;
int MpfArgHasName( int *Argc, char **argv, int rflag, const char *name );
int MpfArgGetInt( int  *Argc, char **argv, int rflag, const char *name, int  *val );
int MpfArgGetString( int *Argc, char **argv, int rflag, const char *name, char **val );
Time get_time(void);
Time time_interval(Time t1, Time t2);
Time add_times(Time t1, Time t2);
double time_in_s(Time t);
double time_interval_in_seconds(Time t1, Time t2);
