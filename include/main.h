#pragma once
#include "config.h"
#include <math.h>
#include <complex.h>
#include <errno.h>
#include <stdbool.h>
#include <limits.h>
#include <pthread.h>
#include "hmat/hmat.h"
#include "util.h"

extern char * batch_input;

extern char * batch_output;

extern double computation_time;

extern size_t ram_usage_peak;

extern double solution_relative_error;

extern bool is_parent_running;

extern hmat_interface_t * interface;

extern double epsilon;

extern hmat_progress_t progress;

/*! \brief Number of right hand sides for the test */
extern int nbRHS;

/*! \brief Sparsity of the right hand sides (1 non-zero value every 'n' values). Default is nbpts/80.log10(nbpts).

The objective of using 'sparseRHS' is to speed-up the computation in produitClassique() in classic.c.
*/
extern int sparseRHS;

/*! \brief Number of points in the points cloud (BEM+FEM) */
extern int nbPts;

/*! \brief RHS for my tests */
extern void *rhs;

/*! \brief  Wavelength (for oscillatory kernels). */
extern double lambda;

/*! \brief User context for building MPF matrices and vectors in testMPF */
typedef struct {
  /*! \brief Pointer on the data used to assembly Mat/vec with computeDenseBlockRHS() */
  void *dataVec;
  /*! \brief Offset on row and column when building sub-part of the FEM-BEM matrix */
  int rowOffset, colOffset;
  /*! \brief Dimensions in columns of the matrix

      Used in prepare_hmat() - and indirectly in computeDenseBlockFEMBEM()
  */
  int colDim;
} contextTestFEMBEM;

int computeKernelBEM(double *coord1, double *coord2, int self, double *kernel) ;
int produitClassiqueBEM(int ffar, void *sol) ;
int produitClassique(void **sol) ;
int computeRelativeError(void *sol, void *ref, double *eps) ;
int computeVecNorm(void *ref, double *norm);
int computeCoordCylinder(int i, double *coord) ;
int initCylinder() ;
void resetCylinder();
int getMeshStep(double *m) ;
int main(int argc, char **argv) ;
int computeRhs(void) ;
double getTime() ;
int displayArray(char *s, void *f, int m, int n) ;
int testHMAT(double * relative_error);
double* createCylinder(void) ;
int printHelp() ;
double compression_to_epsilon(const char * compression);
int run_one();
int run_batch();
void reset_meters();
int get_vmrss(FILE * input, size_t * vmrss);
void * rss_loop(void * __dummy);
void prepare_hmat(int, int, int, int, int*, int*, int*, int*, void*, hmat_block_info_t *);
void advanced_compute_hmat(struct hmat_block_compute_context_t*);
void update_progress(hmat_progress_t * ctx);

struct HMAT_desc_s;
typedef struct HMAT_desc_s HMAT_desc_t;

struct HMAT_desc_s {
  hmat_matrix_t               *hmatrix;
  hmat_clustering_algorithm_t *clustering;
  hmat_cluster_tree_t         *cluster_tree;
  hmat_admissibility_t        *admissibilityCondition;
  contextTestFEMBEM           *myCtx;
};

HMAT_desc_t *HMAT_generate_matrix( hmat_interface_t *hi );
void         HMAT_destroy_matrix ( hmat_interface_t *hi,
                                   HMAT_desc_t      *hdesc );

