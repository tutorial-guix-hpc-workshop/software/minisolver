#include "main.h"

/*! \brief Print help
  \return 0 for success
*/
int printHelp() {

  printf("\ntest_FEMBEM usage:\n-----------------\n\n"
         "     -nbpts: Number of unknowns of the global linear system. Default: 16000 (can be in floating point notation, like 1.2e4).\n"
         "     -epsilon: Compression epsilon. Default: 1e-3.\n"
         "\n"
         "     -h/--help: Display this help\n\n");

  return 0;
}
