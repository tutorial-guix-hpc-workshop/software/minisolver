#include "main.h"

/**
  This structure is a glue between hmat library and application.
*/
typedef struct {
  int row_start;
  int col_start;
  int row_count;
  int col_count;
  int leadingDim;
  int* row_hmat2client;
  int* col_hmat2client;
  double (*row_points)[3];
  double (*col_points)[3];
  void* user_context;
  // To tag dofs used
  int *dof_col_used, *dof_row_used;
} block_data_t;

static void free_block_data(void *v) {
  block_data_t* p = (block_data_t*) v;
  if (p->col_points) free(p->col_points);
  if (p->row_points) free(p->row_points);
  if (p->dof_col_used) free(p->dof_col_used);
  if (p->dof_row_used) free(p->dof_row_used);
  if (p) free(p);
}

/** Callback for hmat block info */
static char is_guaranteed_null_row(const hmat_block_info_t * info, int index, int stratum) {
  block_data_t* b_data = (block_data_t*) info->user_data;
  ASSERTA(b_data);
  ASSERTA(b_data->dof_row_used);
  ASSERTA(index>=0 && index<b_data->row_count);
  ASSERTA (stratum == -1);
  return(!b_data->dof_row_used[index]);
}

/** Callback for hmat block info */
static char is_guaranteed_null_col(const hmat_block_info_t * info, int index, int stratum) {
  block_data_t* b_data = (block_data_t*) info->user_data;
  ASSERTA(b_data);
  ASSERTA(b_data->dof_col_used);
  ASSERTA(index>=0 && index<b_data->col_count);
  ASSERTA (stratum == -1);
  return(!b_data->dof_col_used[index]);
}

/**
  prepare_hmat is called by hmat library to prepare assembly of
  a cluster block.  We allocate a block_data_t, which is then
  passed to the advanced_compute_hmat function.  We have to store all
  datas needed by advanced_compute_hmat into this block_data_t structure.
*/
void
prepare_hmat(int row_start,
             int row_count,
             int col_start,
             int col_count,
             int *row_hmat2client,
             int *row_client2hmat,
             int *col_hmat2client,
             int *col_client2hmat,
             void *user_context,
             hmat_block_info_t * info)
{
  // silent unused variables warning
  (void)row_client2hmat;
  ASSERTA(user_context);
  contextTestFEMBEM *myCtx = (contextTestFEMBEM *)user_context;

  info->user_data = calloc(1, sizeof(block_data_t));
  block_data_t* bdata = (block_data_t*) info->user_data;

  bdata->row_start       = row_start;
  bdata->col_start       = col_start;
  bdata->row_count       = row_count;
  bdata->col_count       = col_count;
  bdata->leadingDim      = row_count; // default value
  bdata->row_hmat2client = row_hmat2client;
  bdata->col_hmat2client = col_hmat2client;
  bdata->user_context    = (void*) user_context;
  info->release_user_data = free_block_data;

  /* Arrays to store non-null rows and columns */
  bdata->dof_row_used = calloc(row_count, sizeof(int)); CHKPTRA(bdata->dof_row_used);
  bdata->dof_col_used = calloc(col_count, sizeof(int)); CHKPTRA(bdata->dof_col_used);

  /* precompute and store the coordinates of the points (rows and columns)
     and tag BEM rows and columns as non-null
  */
  int i, j, col, row, ierr;
  bdata->row_points=(double(*)[3])calloc(row_count, 3*sizeof(double)); CHKPTRA(bdata->row_points);
  for (i = 0; i < row_count; ++i) {
    row = bdata->row_hmat2client ? bdata->row_hmat2client[i + bdata->row_start] : i + bdata->row_start;
    int row_g = row + myCtx->rowOffset;
    if (row_g < nbPts) {
      ierr=computeCoordCylinder(row_g, (double*)&(bdata->row_points[i])); CHKERRA(ierr);
      bdata->dof_row_used[i]=1; // tag this row as non-null
    }
  }
  bdata->col_points=(double(*)[3])calloc(col_count, 3*sizeof(double)); CHKPTRA(bdata->col_points);
  for (j = 0; j < col_count; ++j) {
    col = bdata->col_hmat2client ? bdata->col_hmat2client[j + bdata->col_start] : j + bdata->col_start;
    int col_g = col + myCtx->colOffset;
    if (col_g < nbPts) {
      ierr=computeCoordCylinder(col_g, (double*)&(bdata->col_points[j])); CHKERRA(ierr);
      bdata->dof_col_used[j]=1; // tag this column as non-null
    }
  }

  info->is_guaranteed_null_col = is_guaranteed_null_col;
  info->is_guaranteed_null_row = is_guaranteed_null_row;

}

/**
  Compute all values of a block and store them into an array,
  which had already been allocated by hmat.  There is no
  padding, all values computed in this block are contiguous,
  and stored in a column-major order.
  This block is not necessarily identical to the one previously
  processed by prepare_hmat, it may be a sub-block.  In fact,
  it is either the full block, or a full column, or a full row.
*/
void
advanced_compute_hmat(struct hmat_block_compute_context_t *b) {
  int i, j, ierr;
  double *dValues = (double *) b->block;
  block_data_t* bdata = (block_data_t*) b->user_data;
  int step = 1;
  double (*row_point)[3];
  double (*col_point)[3];

  ASSERTA(bdata->user_context);
  contextTestFEMBEM *myCtx = (contextTestFEMBEM *)bdata->user_context;

  col_point=&(bdata->col_points[b->col_start]);
  for (j = 0; j < b->col_count; ++j, col_point++) { // We could restrict to the col tagged in bdata->dof_col_used[]
    int col = bdata->col_hmat2client ? bdata->col_hmat2client[j + b->col_start + bdata->col_start] : j + b->col_start + bdata->col_start;
    int col_g = col + myCtx->colOffset;

    row_point=&(bdata->row_points[b->row_start]);
    for (i = 0; i < b->row_count; ++i, dValues+=step, row_point++) { // Idem with dof_row_used[]
      int row = bdata->row_hmat2client ? bdata->row_hmat2client[i + b->row_start + bdata->row_start] : i + b->row_start + bdata->row_start;
      int row_g = row + myCtx->rowOffset;
      double Aij = 0;

      // BEM part
      if (row_g < nbPts && col_g < nbPts) {
        ierr=computeKernelBEM((double*)row_point, (double*)col_point, row_g==col_g, &Aij); CHKERRA(ierr);
      }

      *((double*)dValues) = Aij;
    } /* for (i = 0; ... */

    // Advance to the start of the next column
    dValues += step * (bdata->leadingDim-bdata->row_count);

  } /* for (j = 0; ... */
}

HMAT_desc_t *HMAT_generate_matrix( hmat_interface_t *hi ) {

  HMAT_desc_t *hdesc = calloc( 1, sizeof(HMAT_desc_t) );

  /* Create a cluster tree with a clustering algorithm */
  hmat_clustering_algorithm_t *clustering = NULL;
  hmat_cluster_tree_builder_t *ct_builder;

  clustering = hmat_create_clustering_median();
  hmat_set_clustering_divider(clustering, 2);
  ct_builder = hmat_create_cluster_tree_builder( clustering );

  double *points = createCylinder();
  hmat_cluster_tree_t *cluster_tree = hmat_create_cluster_tree_from_builder( points, 3, nbPts,
                                                                             ct_builder );
  free(points); points=NULL;
  hmat_delete_cluster_tree_builder( ct_builder );

  /* Create the H-matrix with this cluster tree and an admissibility criteria */
  hmat_admissibility_t *admissibilityCondition;
  
  hmat_admissibility_param_t admissibility_param;
  hmat_init_admissibility_param(&admissibility_param);
  admissibility_param.eta = 3.0;
  admissibilityCondition = hmat_create_admissibility(&admissibility_param);

  //  hmat_admissibility_t *admissibilityCondition = hmat_create_admissibility_standard(3.0);
  hmat_matrix_t* hmatrix = hi->create_empty_hmatrix_admissibility(cluster_tree, cluster_tree, 1, admissibilityCondition);

  hi->set_low_rank_epsilon(hmatrix, epsilon);

  /* Assembly the Matrix */
  struct mpf_hmat_create_compression_args_t compression_ctx;
  compression_ctx.method = hmat_compress_aca_plus;
  compression_ctx.threshold = epsilon;
  compression_ctx.output = hmat_create_compression_aca_plus(compression_ctx.threshold);

  hmat_assemble_context_t ctx;
  hmat_assemble_context_init(&ctx);
  ctx.progress = &progress;
  ctx.progress->user_data = NULL;
  ctx.factorization = hmat_factorization_none;
  ctx.lower_symmetric = 1;
  ctx.compression = (hmat_compression_algorithm_t*) compression_ctx.output;
  ctx.prepare = prepare_hmat;
  ctx.advanced_compute = advanced_compute_hmat;
  contextTestFEMBEM *myCtx = calloc(1, sizeof(contextTestFEMBEM)) ; CHKPTRA(myCtx);
  myCtx->colDim = nbPts;
  ctx.user_context = myCtx;
  hi->assemble_generic(hmatrix, &ctx);

  hmat_delete_compression(ctx.compression);

  hdesc->clustering = clustering;
  hdesc->cluster_tree = cluster_tree;
  hdesc->admissibilityCondition = admissibilityCondition;
  hdesc->myCtx = myCtx;
  hdesc->hmatrix = hmatrix;

  return hdesc;
}

void HMAT_destroy_matrix( hmat_interface_t *hi,
                          HMAT_desc_t      *hdesc )
{
  /* Destroys the HMAT structure */
  if (hdesc) {
    hi->destroy( hdesc->hmatrix );
    hmat_delete_admissibility(hdesc->admissibilityCondition);
    hmat_delete_cluster_tree(hdesc->cluster_tree);
    hmat_delete_clustering(hdesc->clustering);
    if (hdesc->myCtx) free(hdesc->myCtx);
    free(hdesc);
  }
}
