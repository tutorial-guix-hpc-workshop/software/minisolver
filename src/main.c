#include "main.h"

char * batch_input = NULL;
char * batch_output = NULL;
double computation_time = 0.;
size_t ram_usage_peak = 0UL;
double solution_relative_error = 0.;
bool is_parent_running = false;
hmat_interface_t * interface = NULL;
double epsilon = 1e-3;
hmat_progress_t progress;
int nbRHS = 1;
int sparseRHS;
int nbPts = 16000;
void *rhs = NULL;
double lambda;

void update_progress(hmat_progress_t * ctx) {
  Mpf_progressBar(ctx->current, ctx->max);
}

int get_vmrss(FILE * input, size_t * vmrss) {
  char * buffer = (char *) malloc(1024); CHKPTRQ(buffer);
  while((buffer = fgets(buffer, 1023, input))) {
    if(strncmp(buffer, "VmRSS:", 6) == 0) {
      int ret = sscanf(buffer, "VmRSS: %lu", vmrss);
      if(ret == EOF) {
        SETERRQ(1, "Unable to read the value of VmRSS in /proc/self/status!");
      }
    }
  }
  free(buffer);
  return 0;
}

void * rss_loop(void * __dummy) {
  int ierr;
  while(is_parent_running) {
    FILE * parent = fopen("/proc/self/status", "r"); CHKPTRA(parent);
    size_t VmRSS = 0UL;
    ierr = get_vmrss(parent, &VmRSS); CHKERRA(ierr);
    if(VmRSS > ram_usage_peak) {
      ram_usage_peak = VmRSS;
    }
    fclose(parent);
    sleep(1);
  }
  return NULL;
}

double compression_to_epsilon(const char * compression) {
  if(strncmp(compression, "medium", 6) == 0) {
    return 1e-6;
  } else if(strncmp(compression, "low", 3) == 0) {
    return 1e-10;
  } else if(strncmp(compression, "disabled", 8) == 0) {
    return 1e-16;
  }

  return 1e-3;
}

void reset_meters() {
  computation_time = 0.;
  ram_usage_peak = 0.;
  solution_relative_error = 0.;
}

int run_batch() {
  if(!batch_input) {
    SETERRQ(
      1,
      "Batch file name was not initialized! Did you forget the --batch option?"
    );
  }

  // Open the batch file for reading.
  FILE * input = fopen(batch_input, "r"); CHKPTRQ(input);

  // Open the output file to save the test results in.
  FILE * output = fopen(batch_output, "w"); CHKPTRQ(output);

  int c = 0, ierr, ret = 0;
  char * compression = (char *) malloc(9); CHKPTRQ(compression);

read:
  compression = memset(compression, 0x0, 9); CHKPTRQ(compression);
  c = fscanf(input, "%d,%s\n", &nbPts, compression);
  if(c == EOF && errno) {
    SHWERRM(
      1, 
      "Failed to read the input batch file! Execution cannot continue!"
    );
    ret = 1;
    goto end;
  }
  if(c == EOF) {
    goto end;
  }
  if(c < 2) {
    SHWERRM(
      1, 
      "A syntax error occured while reading the input batch file! "
      "Execution cannot continue!"
    );
    ret = 1;
    goto end;
  }
  printf("COMPRESSION IS %s\n", compression);
  epsilon = compression_to_epsilon(compression);
  printf("EPSILON IS = %e\n", epsilon);
  reset_meters();
  run_one();
  ierr = fprintf(
    output, "%d,%s,%f,%lu,%e\n",
    nbPts, compression, computation_time, ram_usage_peak / 1024UL,
    solution_relative_error
  );
  if(ierr < 0) {
    SHWERRM(
      1, 
      "Failed to write the result into the output file! "
      "Execution cannot continue!"
    );
    ret = 1;
    goto end;
  }
  goto read;

end:
  fclose(input);
  fclose(output);
  free(compression);

  return ret;
}

int run_one() {
  int ierr;
  pthread_t child;

  is_parent_running = true;

  ierr = pthread_create(&child, NULL, rss_loop, NULL); CHKERRQ(ierr);

  printf("[minisolver] hmat initialization ... ");
  interface = calloc(1, sizeof(hmat_interface_t)); CHKPTRQ(interface);
  hmat_init_default_interface(interface, HMAT_DOUBLE_PRECISION);
  if (!interface->init)
      SETERRQ(1, "No valid HMatrix interface. Incorrect parameters ?");
  interface->init();
  progress.update = update_progress;
  printf("done\n");

  printf("[minisolver] preparing test ... ");
  resetCylinder();
  ierr=initCylinder(); CHKERRQ(ierr);

  /* Wavelength */
  double step;
  ierr = getMeshStep(&step);
  lambda = 10.*step;

  /* Setting remaining variables */
  sparseRHS=(double)nbPts/80./log10((double)nbPts) ;
  if (sparseRHS<1) sparseRHS = 1;

  /* Cree le second membre du produit mat-vec */
  rhs = calloc((size_t) nbPts * nbRHS, sizeof(double)); CHKPTRQ(rhs);
  ierr = computeRhs(); CHKERRQ(ierr);
  printf("done\n");

  printf("[minisolver] lambda = %f\n", lambda) ;
  printf("[minisolver] right-hand side sparsity = %d\n", sparseRHS) ;
  
  double relative_error;
  ierr = testHMAT(&relative_error); CHKERRQ(ierr) ;
  is_parent_running = false;

  printf("[minisolver] computation completed\n");

  printf("[minisolver] cleaning ... ");
  if (rhs) {
    free(rhs);
    rhs = NULL;
  }
  printf("done\n");

  printf("[minisolver] hmat finalization ... ");
  interface->finalize();
  free(interface);
  printf("done\n");

  pthread_join(child, NULL);

  return 0;
}

/*! \brief Main routine
  \return 0 for success
*/
int main(int argc, char **argv) {
  int ierr;
  
  printf("[minisolver] minisolver version = %s\n", PACKAGE_VERSION);
  printf("[minisolver] hmat version = %s\n", HMAT_VERSION);

  if(MpfArgGetString(&argc, argv, 1, "--batch-input", &batch_input)) {
    if(!MpfArgGetString(&argc, argv, 1, "--batch-output", &batch_output)) {
      batch_output = (char *) malloc(12); CHKPTRQ(batch_output);
      batch_output = strncpy(batch_output, "results.csv", 11);
      CHKPTRQ(batch_output);
    }

    printf("[minisolver] batch input file = %s\n", batch_input);
    printf("[minisolver] batch output file = %s\n", batch_output);

    printf("[minisolver] running test batch ... ");
    ierr = run_batch(); CHKERRQ(ierr);
    printf("done\n");

    free(batch_input);
    free(batch_output);

    return 0;
  }

  if (MpfArgGetInt(&argc, argv, 1, "--size", &nbPts)) {
    printf("[minisolver] system size = %d\n", nbPts) ;
  }

  char * compression = NULL;
  if (MpfArgGetString( &argc, argv, 1, "--compression" , &compression)) {
    epsilon = compression_to_epsilon(compression);
  }

  printf("[minisolver] compression = %s\n", compression ? compression : "high");
  printf("[minisolver] epsilon = %.0e\n", epsilon);

  if(compression) {
    free(compression);
  }

  ierr = run_one(); CHKERRQ(ierr);

  printf("[minisolver] total computation time = %f\n", computation_time);
  printf("[minisolver] memory usage peak = %lu MiB\n", ram_usage_peak / 1024UL);
  
  return 0;
}
