#include "main.h"

/*! \brief Runs the test of H-matrices : gemv, solve
 */
int testHMAT(double * relative_error) {
  int ierr;
  double temps_initial, temps_final, temps_cpu;

  /* Realise le produit matrice vecteur classique : A.rhs -> solCLA */

  printf("[minisolver] computing classical product:");
  void *solCLA=NULL;
  ierr = produitClassique(&solCLA); CHKERRQ(ierr);

  /* Cree la H-Matrice */

  temps_initial = getTime ();
  printf("[minisolver] creating matrix ... ");

  HMAT_desc_t *hdesc;
  hdesc = HMAT_generate_matrix( interface );
  hmat_matrix_t *hmatrix = hdesc->hmatrix;
  temps_final = getTime();
  printf("done\n");

  temps_cpu = temps_final - temps_initial;
  printf("[minisolver] matrix creation time = %f\n", temps_cpu);

  /* Factorize the H-matrix */

  printf("[minisolver] factorizing matrix:");
  temps_initial = getTime();

  hmat_factorization_context_t ctx;
  hmat_factorization_context_init(&ctx);
  ctx.progress = &progress;
  ctx.progress->user_data = NULL;
  ctx.factorization = hmat_factorization_ldlt;
  ierr = interface->factorize_generic(hmatrix, &ctx); CHKERRQ(ierr);

  temps_final = getTime();
  temps_cpu = (temps_final - temps_initial) ;
  computation_time += temps_cpu;
  printf("[minisolver] factorization time = %f\n", temps_cpu) ;

  /* Solve the system : A-1.solCLA -> solCLA */

  printf("[minisolver] solving matrix ... ");

  temps_initial = getTime();

  ierr = interface->solve_systems(hmatrix, solCLA, nbRHS); CHKERRQ(ierr);

  temps_final = getTime();
  printf("done\n");

  temps_cpu = (temps_final - temps_initial);
  computation_time += temps_cpu;
  printf("[minisolver] solve time = %f\n", temps_cpu);

  /* Compare the two vectors solCLA and rhs */

  printf("[minisolver] computing relative error ... ");
  ierr=computeRelativeError(solCLA, rhs, relative_error); CHKERRQ(ierr);
  solution_relative_error = *relative_error;
  printf("done\n");

  printf("[minisolver] relative error = %.4e\n", *relative_error);

  printf("[minisolver] test-specific cleaning ... ");
  HMAT_destroy_matrix( interface, hdesc );
  free(solCLA);
  printf("done\n");

  return 0;
}
