#include "main.h"

/*! \brief Realise le produit matrice-vecteur classique, parallele, pour les interactions BEM

  \param ffar vaut 1 si on prend que le lointain, 0 si on prend que le proche, 2 si on prend tout
  \param sol the output vector. Must be allocated and filled with zeros
  \param myComm an MPI Communicator
  \return 0 for success
*/
int produitClassiqueBEM(int ffar, void *sol) {
  int jg, ierr;

  if (nbPts==0) return 0;

  /* Global counter jg = only for progress bar */
  jg=0;
  ierr=Mpf_progressBar(0, nbPts+1) ; CHKERRA(ierr);
  /* Loop on the columns of the matrix that correspond to non-zero rows of the RHS */
  for (int j=0 ; j<nbPts ; j+=sparseRHS) {
    int i, k, ierr;
    double coord_i[4], coord_j[3];
    double Aij ;
    double *d_sol=sol, *d_rhs=rhs ;

    ierr=computeCoordCylinder(j, &(coord_j[0])) ; CHKERRA(ierr);
    /* Boucle sur les lignes */
    for (i=0 ; i<nbPts ; i++) {
      ierr=computeCoordCylinder(i, &(coord_i[0])) ; CHKERRA(ierr);
      ierr=computeKernelBEM(&(coord_i[0]), &(coord_j[0]), i==j, &Aij) ;
      for (k=0 ; k<nbRHS ; k++) {
        d_sol[(size_t)k*nbPts+i] += Aij * d_rhs[(size_t)k*nbPts+j] ;
      }
    } /* for (i=0 */

    int jg_bak;
    jg_bak = jg+=sparseRHS ;

    ierr=Mpf_progressBar(jg_bak+1, nbPts+1) ; CHKERRA(ierr);
  }

  return 0 ;
}

/* Allocates an array and stores in it the mat-vec product A.rhs -> sol computed "classicaly" */
int produitClassique(void **sol) {
  int ierr;
  double temps_initial, temps_final, temps_cpu;

  void *solCLA = calloc((size_t)nbPts*nbRHS, sizeof(double)) ; CHKPTRQ(solCLA) ;

  temps_initial = getTime ();
  ierr = produitClassiqueBEM(2, solCLA) ; CHKERRQ(ierr);
  temps_final = getTime ();
  temps_cpu = temps_final - temps_initial ;
  printf("[minisolver] classical product time = %f\n", temps_cpu) ;
  *sol = solCLA;

  return 0;
}
