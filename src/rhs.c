#include "main.h"

extern double height;

int computeRhs(void) {
  double coord[3];
  int i, j, ierr ;
  double *d_rhs=rhs ;
  double small = 1.e-100;

  for (i=0 ; i<nbPts ; i++)
    if (i % sparseRHS == 0) {
      // Insert non-zero values on 1 value every 'sparseRHS'
      ierr = computeCoordCylinder(i, &(coord[0])) ; CHKERRQ(ierr);
      for (j=0 ; j<nbRHS ; j++) {
        d_rhs[(size_t)j*nbPts+i]=cos(coord[0]/(j+1)+coord[1]+sin(j)*coord[2])+sin(j) ;
      }
    } else {
      // Insert zero everywhere else (in fact we put a 'very small' value
      // instead of zero, otherwise run-time optimisations produce unrealistic timers with FMM)
      for (j=0 ; j<nbRHS ; j++) {
        d_rhs[(size_t)j*nbPts+i]=small ;
      }
    }

  return 0 ;
}
