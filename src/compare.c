#include "main.h"

static inline void
frobenius_update( double *scale, double *sumsq, const double value )
{
  double absval = fabs(value);
  double ratio;
  if ( absval != 0. ){
    if ( (*scale) < absval ) {
      ratio = (*scale) / absval;
      *sumsq = (double)1. + (*sumsq) * ratio * ratio;
      *scale = absval;
    } else {
      ratio = absval / (*scale);
      *sumsq = (*sumsq) + ratio * ratio;
    }
  }
}

int computeRelativeError(void *sol, void *ref, double *eps) {
  double normRef[2], normDelta[2];
  double *d_sol=sol, *d_ref=ref ;

  *eps=0.;
  normDelta[0] = 1.;
  normDelta[1] = 0.;
  normRef[0]   = 1.;
  normRef[1]   = 0.;
  for (size_t i=0 ; i<(size_t)nbPts*nbRHS ; i++) {
    frobenius_update( normDelta, normDelta+1, (d_sol[i] - d_ref[i]) );
    frobenius_update( normRef, normRef+1, d_ref[i] );
  }
  if ( normDelta[1] == 0. ) {/* To handle correctly the case where both vectors are null */
    *eps = 0.;
  }
  else {
    *eps = (normDelta[0] * sqrt( normDelta[1] ) )
        /  (normRef[0]   * sqrt( normRef[1]   ) );
  }

  return 0 ;
}

int computeVecNorm(void *ref, double *norm) {
  double normRef ;
  double *d_ref=ref ;

  *norm=0.;
  normRef=0. ;
  for (size_t i=0 ; i<(size_t)nbPts*nbRHS ; i++) {
    normRef += d_ref[i]*d_ref[i] ;
  }
  *norm = sqrt(normRef) ;

  return 0 ;
}
