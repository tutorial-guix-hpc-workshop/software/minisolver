#include "main.h"

/* ============================================================================================================ */

/*! \brief Radius of the cylinder */
double radius = 2.0 ;

/*! \brief Height of the cylinder */
double height = 4.0 ;

/*! \brief Mesh step (average distance between two points) */
double meshStep;

static double angleStep=0.;

static double zStep=0.;

/*! \brief Computes coordinates of point number 'i'

  The points are computed on a cylinder defined by 'radius' and 'height'. The number of points is given by nbPtsMain.
  Points are located on a helix, equally spaced in direction z and orthoradial.
  \a i should be in [0, nbPtsMain[.
  \param i the number of the point (input)
  \param coord the coordinates x y z of this point (output)
  \return 0 for success
*/
int computeCoordCylinder(int i, double *coord) {
  double theta ;

  if (i<0 || i>=nbPts) {
    SETERRQ(1, "Incorrect unknown number %d, should be in [0, %d[\n", i, nbPts) ;
  }

  if (angleStep==0.) {
    /* zStep and angleStep are defined by :
       angleStep*radius = zStep*nbPtsLoop (= meshStep)
       angleStep*nbPtsLoop = 2.pi */
    zStep=height/(double)nbPts ;
    angleStep=sqrt(zStep*2.*M_PI/radius) ;
    meshStep=zStep*2.*M_PI/angleStep ;
  }

  theta = (double)i * angleStep ;
  coord[0] = radius * sin(theta) ;
  coord[1] = radius * cos(theta) ;
  coord[2] = (double)i*zStep ;

  return 0 ;
}

double* createCylinder(void) {
  double* result = calloc((size_t)3 * nbPts, sizeof(double));
  int i, ierr;
  for (i = 0; i < nbPts; i++) {
    ierr=computeCoordCylinder(i, &(result[(size_t)3*i]) ) ; CHKERRA(ierr) ;
  }

  return result;
}

/*! \brief Reads command line arguments defining the shape of the cylinder

  \return 0 for success
*/
int initCylinder() {
  int ierr ;
  double coord[3] ;

  /* Calcule les coordonnees d'un point, afin de fixer meshStep (qui peut servir a fixer lambda) */
  ierr=computeCoordCylinder(0, &(coord[0])) ; CHKERRQ(ierr) ;

  return 0 ;
}

void resetCylinder() {
  angleStep=0.;
  zStep=0.;
}

int getMeshStep(double *m) {
  if (meshStep==0.)
    SETERRQ(1, "meshStep not yet initialized. Come back later.") ;

  *m = meshStep ;
  return 0 ;
}