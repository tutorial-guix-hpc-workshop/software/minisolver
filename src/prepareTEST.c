#include "main.h"

double getTime() {
  return (time_in_s(get_time())) ;
}

int displayArray(char *s, void *f, int m, int n) {
  int i, j ;
  //double _Complex *z_f=f ;
  double *d_f=f ;

  for (j=0 ; j<n ; j++)
    for (i=0 ; i<m ; i++)
      //if (complexALGO)
        //printf("%s[%d, %d]=(%e, %e)\n", s, i, j, z_f[(size_t)j*m+i].r, z_f[(size_t)j*m+i].i) ;
      //else
        printf("%s[%d, %d]=%e\n", s, i, j, d_f[(size_t)j*m+i]) ;
  return 0 ;
}

