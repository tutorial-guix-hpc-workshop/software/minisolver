#include "main.h"
extern double meshStep;
int computeKernelBEM(double *coord1, double *coord2, int self, double *kernel) {
  double r, v[3];

  v[0]=coord1[0]-coord2[0] ;
  v[1]=coord1[1]-coord2[1] ;
  v[2]=coord1[2]-coord2[2] ;
  r=sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]) ;
  if (r<meshStep/2.) r=meshStep/2.;

  *kernel = 1 / (4 * M_PI * r);

  return 0;
}
