;; Nasledovné predstavuje „balíkospis“ zhodný s príkazovým riadkom, ktorý ste zadali.
;; Môžete si ho uložiť do súboru a potom ho použiť s ktorýmkoľvek „guix“ príkazom
;; prijímajúcim voľbu „--manifest“ (alebo „-m“).

(specifications->manifest
  (list "bash"
        "coreutils"
        "gcc-toolchain"
        "gdb"
        "git"
        "cmake"
        "make"
        "nano"
        "emacs"
        "emacs-cmake-mode"
        "emacs-spacemacs-theme"
        "emacs-org"
        "emacs-org-ref"
        "emacs-setup"
        "emacs-projectile"
        "emacs-fill-column-indicator"
        "valgrind"
        "openblas"
        "hmat-oss"
        "openmpi"
        "openssh"
        "pkg-config"))
