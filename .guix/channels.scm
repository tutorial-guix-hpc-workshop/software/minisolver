(list
 (channel
  (name 'guix)
  (url "https://git.savannah.gnu.org/git/guix.git")
  (branch "master")
  (commit
   "58786c82df700fb1f1c30022ee6ec0b2e7516e7b"))
 (channel
  (name 'guix-hpc)
  (url "https://gitlab.inria.fr/guix-hpc/guix-hpc.git")
  (branch "master")
  (commit "6cfd2ca675d2f9a5f7a7faa348e7f311757cb35f"))
 (channel
  (name 'guix-hpc-non-free)
  (url "https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free.git")
  (branch "master")
  (commit "1c273f17fd5d57e3626067c06b38966a9c63d652")))
